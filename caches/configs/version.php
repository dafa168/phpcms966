<?php
if (!defined('IN_CMS')) exit('No direct script access allowed');
return array(
'pc_version' => 'V9.6.3', //版本号
'pc_release' => '20170515', //更新日期
'cms_version' => 'V10.0.0', //cms 版本号
'cms_release' => '20231216', //cms 更新日期
'cms_updatetime' => '2023-12-16', // 服务端最近更新时间
'cms_downtime' => '2023-12-16 16:06:06', // 本网站程序下载时间
'update' => '0', //cms 更新
);
?>