<?php
/**
 * 生成Tag URL
 * @param $tid
 * @return string
 */
function tag_url($keyword, $siteid = '', $catid = ''){
	!$siteid && $siteid = get_siteid();
	return $catid ? APP_PATH.'index.php?m=content&c=search&a=init&catid='.$catid.'&dosubmit=1&info%5Bcatid%5D='.$catid.'&info%5Btypeid%5D=0&info%5Btitle%5D='.urlencode($keyword) : APP_PATH.'index.php?m=content&c=tag&a=lists&tag='.urlencode($keyword).'&siteid='.$siteid;
}
?>